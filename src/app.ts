import * as http from "http";

const server = http.createServer((req, res) => {
    res.setHeader("Content-Type", "text/plain");
    res.write("Hello World");
    res.end();
});

server.listen(3000);
