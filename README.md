# TypeScript Node HTTP Server Boilerplate
Simple starting point for building node.js HTTP servers in TypeScript.

## Installation
### Prerequisites
1. node installed (tested with version 4.4.7)
2. npm installed (tested with version 2.15.8)

### Instructions
1. Clone repository
2. Open terminal to project root
3. Execute "npm install"
4. Execute "npm run build"
5. Exectue "npm start"
6. Open browser and navigate to "http://localhost:3000/"